﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text.RegularExpressions;
using System.Threading;

namespace TaskLogger
{
    internal class Program
    {
        // P/Invoke declarations for Console window position
        [DllImport("kernel32.dll")]
        private static extern IntPtr GetConsoleWindow();

        [DllImport("user32.dll")]
        private static extern IntPtr MonitorFromWindow(IntPtr hwnd, uint dwFlags);

        private const int MONITOR_DEFAULTTOPRIMARY = 1;

        [DllImport("user32.dll")]
        private static extern bool GetMonitorInfo(IntPtr hMonitor, ref MONITORINFO lpmi);

        [StructLayout(LayoutKind.Sequential)]
        private struct MONITORINFO
        {
            public uint cbSize;
            public RECT rcMonitor;
            public RECT rcWork;
            public uint dwFlags;

            public static MONITORINFO Default
            {
                get { var inst = new MONITORINFO(); inst.cbSize = (uint)Marshal.SizeOf(inst); return inst; }
            }
        }

        [StructLayout(LayoutKind.Sequential)]
        private struct RECT
        {
            public int Left, Top, Right, Bottom;
        }

        [StructLayout(LayoutKind.Sequential)]
        private struct POINT
        {
            public int x, y;
        }

        [DllImport("user32.dll", SetLastError = true)]
        private static extern bool GetWindowPlacement(IntPtr hWnd, ref WINDOWPLACEMENT lpwndpl);

        [DllImport("user32.dll", SetLastError = true)]
        private static extern bool SetWindowPlacement(IntPtr hWnd, [In] ref WINDOWPLACEMENT lpwndpl);

        private const uint SW_RESTORE = 9;

        [StructLayout(LayoutKind.Sequential)]
        private struct WINDOWPLACEMENT
        {
            public uint Length;
            public uint Flags;
            public uint ShowCmd;
            public POINT MinPosition;
            public POINT MaxPosition;
            public RECT NormalPosition;

            public static WINDOWPLACEMENT Default
            {
                get
                {
                    var instance = new WINDOWPLACEMENT();
                    instance.Length = (uint)Marshal.SizeOf(instance);
                    return instance;
                }
            }
        }

        private static void Main(string[] args)
        {
            // todo
            // log dateTime, task and duration              done
            // only log if task time is over                done
            // category                                     done
            // show time until finish - progressbar         done
            // stats about the last days

            var taskDict = new Dictionary<string, string[]>();
            taskDict.Add("Programming", new string[] { "C#", "Cucumber", "Database", "LINQ",
                "Regex", "Specflow", "Gherkin", "Pickles", "Selenium", "NUnit", "git", "ContinousIntegration" });
            taskDict.Add("Reading", new string[] { "ADHD", "Giftedness", "HSP", "Novel", "MBTI" });
            taskDict.Add("Training", new string[] { "Self-Employment", "Math", "Physics", "Anki" });
            taskDict.Add("Sport", new string[] { "Running", "Basketball", "Walk" });
            taskDict.Add("Children", new string[] { "BringToBed" });
            taskDict.Add("Cleaning", new string[] { "MyRoom", "WashTheDishes", "Hallway" });
            taskDict.Add("Break", new string[] { "Break", "Coffee", "Music", "Zazen", "JustSitting",
                "WatchBreath", "Sleep" });

            MoveConsoleWindowToLowerLeft();

            // print all possible categories
            Console.WriteLine("Choose task category:");
            var myCategories = new List<string>();
            foreach (var item in taskDict)
            {
                Console.WriteLine(item.Key);
                myCategories.Add(item.Key);
            }
            Console.WriteLine();

            // auto-completion
            var myCategory = "";
            Console.Write("Your category: ");
            for (int i = 0; i < 50; i++)
            {
                myCategory += Console.ReadKey().KeyChar;

                // search in myCategories
                var found = myCategories.Where(x => x.ToLower().StartsWith(myCategory)).Select(x => x).ToList();
                if (found.Count == 1)
                {
                    Console.WriteLine(found[0].Substring(myCategory.Length, found[0].Length - myCategory.Length));
                    myCategory += found[0].Substring(myCategory.Length, found[0].Length - myCategory.Length);
                    break;
                }
            }

            // capitalize
            myCategory = myCategory.First().ToString().ToUpper() + myCategory.Substring(1);

            // print all possible subtasks
            Console.WriteLine();
            Console.WriteLine("Choose task: ");
            var myTasks = new List<string>();
            foreach (var item in taskDict[myCategory])
            {
                Console.WriteLine(item);
                myTasks.Add(item);
            }
            Console.WriteLine();

            // auto-completion
            var myTask = "";
            Console.Write("Your task: ");
            for (int i = 0; i < 50; i++)
            {
                myTask += Console.ReadKey().KeyChar;

                // search in myCategories
                var found = myTasks.Where(x => x.ToLower().StartsWith(myTask)).Select(x => x).ToList();
                if (found.Count == 1)
                {
                    Console.WriteLine(found[0].Substring(myTask.Length, found[0].Length - myTask.Length));
                    myTask += found[0].Substring(myTask.Length, found[0].Length - myTask.Length);
                    break;
                }
            }

            Console.WriteLine();

            // capitalize
            myTask = myTask.First().ToString().ToUpper() + myTask.Substring(1);

            // start timer
            var tastStartTime = DateTime.Now;
            var durationInMinutes = 8;

            // wait
            ProgressBarWithDurationInSeconds(durationInMinutes * 60);

            BlinkAtFinish();

            Console.Beep(1661, 50);
            Thread.Sleep(20);
            Console.Beep(1174, 50);

            // log to file
            var fileName = @"C:\Users\User\Dropbox\data\task_logger.txt";
            File.AppendAllText(fileName, tastStartTime + "\t"
                + myCategory + myTask + "\t" + durationInMinutes + Environment.NewLine);

            // show task statistic
            ShowStatisticsByTask(fileName);
            ShowStatisticsByHour(fileName);
            ShowStatisticsOfTheLastDays(fileName);
            Console.ReadKey();
        }

        private static void MoveConsoleWindowToLowerLeft()
        {
            // Get this console window's hWnd (window handle).
            IntPtr hWnd = GetConsoleWindow();

            // Get information about the monitor (display) that the window is (mostly) displayed on.
            // The .rcWork field contains the monitor's work area, i.e., the usable space excluding
            // the taskbar (and "application desktop toolbars" - see https://msdn.microsoft.com/en-us/library/windows/desktop/ms724947(v=vs.85).aspx)
            var mi = MONITORINFO.Default;
            GetMonitorInfo(MonitorFromWindow(hWnd, MONITOR_DEFAULTTOPRIMARY), ref mi);

            // Get information about this window's current placement.
            var wp = WINDOWPLACEMENT.Default;
            GetWindowPlacement(hWnd, ref wp);

            // Calculate the window's new position: lower left corner.
            // !! Inexplicably, on W10, work-area coordinates (0,0) appear to be (7,7) pixels
            // !! away from the true edge of the screen / taskbar.
            int fudgeOffset = 7;
            wp.NormalPosition = new RECT()
            {
                Left = -fudgeOffset,
                Top = mi.rcWork.Bottom - (wp.NormalPosition.Bottom - wp.NormalPosition.Top),
                Right = (wp.NormalPosition.Right - wp.NormalPosition.Left),
                Bottom = fudgeOffset + mi.rcWork.Bottom
            };

            // Place the window at the new position.
            SetWindowPlacement(hWnd, ref wp);
        }

        private static void ShowStatisticsByTask(string fileName)
        {
            var fileInput = File.ReadAllText(fileName);
            fileInput = Regex.Replace(fileInput, "[0-9]", "");
            fileInput = Regex.Replace(fileInput, " ", "");
            fileInput = Regex.Replace(fileInput, "..::", "");
            fileInput = Regex.Replace(fileInput, "\n", "");
            fileInput = Regex.Replace(fileInput, "\r", "");
            var gg = fileInput.Split('\t').GroupBy(i => i)
                    .OrderByDescending(g => g.Count());

            foreach (var grp in gg)
            {
                Console.WriteLine($"{grp.Count()}\t{grp.Key}");
            }

            Console.WriteLine();
        }

        private static void ShowStatisticsByHour(string fileName)
        {
            var fileInput = File.ReadAllText(fileName);
            var fileInputArray = fileInput.Split('\n');
            var hourStats = new List<int>();
            for (int i = 0; i < 24; i++)
            {
                hourStats.Add(0);
            }

            foreach (var line in fileInputArray)
            {
                if (line.Contains("Programming") || line.Contains("Training"))
                {
                    // take hour from DateTime
                    DateTime myDate = DateTime.ParseExact(line.Split('\t')[0], "dd.MM.yyyy HH:mm:ss",
                                       System.Globalization.CultureInfo.InvariantCulture);
                    var hour = myDate.Hour;
                    hourStats[hour] += 1;
                }
            }

            Console.WriteLine("Hour   N_Tasks");
            for (int i = 0; i < 24; i++)
            {
                Console.WriteLine(
                    i.ToString() +
                    " ".PadLeft((8 - i.ToString().Length) - hourStats[i].ToString().Length) +
                    hourStats[i]);
            }

            Console.WriteLine();
        }

        private static void ShowStatisticsOfTheLastDays(string fileName)
        {
            var fileInput = File.ReadAllText(fileName);
            var fileInputArray = fileInput.Split('\n');

            var taskDates = new List<string>();

            foreach (var line in fileInputArray)
            {
                if (line.Contains("Programming") || line.Contains("Training"))
                {
                    // take hour from DateTime
                    DateTime myDate = DateTime.ParseExact(line.Split('\t')[0].Split(' ')[0], "dd.MM.yyyy",
                                       System.Globalization.CultureInfo.InvariantCulture);
                    taskDates.Add(myDate.ToString().Split(' ')[0]);
                }
            }

            Console.WriteLine("Date         Amount");
            var res = taskDates.GroupBy(x => x).OrderBy(i => i.Key);
            foreach (var item in res)
            {
                Console.WriteLine(item.Key + "   " + item.Count());
            }
            
            Console.WriteLine();
        }

        private static void BlinkAtFinish()
        {
            for (int j = 0; j < 1; j++)
            {
                Thread.Sleep(1000);
                for (int i = 0; i < 40; i++)
                {
                    Console.WriteLine("##############################################################################");
                    Console.WriteLine("###############                  ##########                   ################");
                }
                Thread.Sleep(1000);
                for (int i = 0; i < 40; i++)
                {
                    Console.WriteLine();
                }
            }
        }

        private static void ProgressBarWithDurationInSeconds(int waitTimeInSeconds)
        {
            var secondsUntilEnd = waitTimeInSeconds;
            var secondsNow = 0;
            for (int i = 0; i < secondsUntilEnd; i++)
            {
                Thread.Sleep(1000);

                secondsNow += 1;
                var percentage = 100.0 * secondsNow / secondsUntilEnd;
                Console.Write("[");
                for (int j = 0; j < Math.Round(percentage); j++)
                {
                    Console.Write("#");
                }
                for (int j = 0; j < 100 - Math.Round(percentage); j++)
                {
                    Console.Write("-");
                }
                Console.Write("]");
                Console.WriteLine();
            }
        }
    }
}